from django.db import models


class Event(models.Model):
    id = models.AutoField(primary_key=True)
    event = models.CharField(max_length=500)
    address = models.TextField()
    date = models.CharField(max_length=10)
    price = models.FloatField()

    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        verbose_name = 'Подія'
        verbose_name_plural = 'Події'

    def __str__(self):
        return '{0}'.format(self.event, self.date)
