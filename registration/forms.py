from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import UserProfile


class UserProfileForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        fields = ['name', 'surname', 'phone_number', 'has_trainer']

        labels = {
            'name': _('Iм’я'),
            'surname': _('Прізвище'),
            'phone_number': _('Номер телефону'),
            'has_trainer': _('Є тренер')
        }
        help_texts = {
            'phone_number': _('Введіть номерв в форматі 38063XXXXXXX'),
            'has_trainer': _('Якщо Ви (Ваші діти) відвідуєте секцію Бойового Гопака поставте галочку навпроти "Є тренер".'),
        }
        error_messages = {

        }


class PaymentForm(forms.Form):
    email = forms.CharField(label='Адреса електронної пошти', max_length=100)
    credit_card_num = forms.IntegerField(label='Номер карти')
    month = forms.IntegerField(label='Місяць (month)', max_value=12)
    year = forms.IntegerField(label='Рік (year)', max_value=3000)
    cvv = forms.IntegerField(label='Секретний код cvv')

