from django.urls import path, include
from . import views


urlpatterns = [
    path('', views.landing_page, name='landing page'),
    path('registration/', views.registration, name='registration'),
    path('for-trainee/', views.for_trainee, name='for trainee'),

    path('not-trainer/', views.payment, name='not trainee'),
    path('paypal_return/', views.paypal_return, name='paypal return'),
    path('paypal_cancel/', views.paypal_cancel, name='paypal cancel'),
]
