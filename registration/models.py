from django.db import models


class UserProfile(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=40)
    phone_number = models.CharField(max_length=12)
    has_trainer = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Заявка на попередню рейстрацію'
        verbose_name_plural = 'Заявки на попередню рейстрацію'

    def __str__(self):
        return '{0} {1}'.format(self.name, self.surname)


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    message = models.TextField()

    class Meta:
        verbose_name = 'Повідомлення для смс розсилки'
        verbose_name_plural = 'Повідомлення для смс розсилки'

    def __str__(self):
        return '{0}'.format(self.id)
