from django.shortcuts import get_object_or_404

from .models import UserProfile
from event.models import Event

from paypal.standard.ipn.signals import valid_ipn_received
from django.dispatch import receiver


@receiver(valid_ipn_received)
def payment_notification(sender, **kwargs):
    ipn = sender
    if ipn.payment_status == 'Completed':
        # payment successful
        ticket = get_object_or_404(UserProfile, id=ipn.invoice)

        event = Event.objects.all()[Event.objects.count() - 1]

        if event.price == ipn.mc_gross:
            # mark the event as paid
            ticket.paid = True
            ticket.save()
