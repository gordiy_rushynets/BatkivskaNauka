import turbosmsua

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse

from .forms import UserProfileForm
from paypal.standard.forms import PayPalPaymentsForm

from .models import UserProfile
from event.models import Event


id = None
phone = None

def registration(request):
    global id, phone

    if request.method == 'POST':

        form = UserProfileForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']
            surname = form.cleaned_data['surname']
            phone = form.cleaned_data['phone_number']

            # minichecing phone number
            if len(phone) == 12 or len(phone) == 10:

                if len(phone) == 10:
                    phone = '38' + phone

                try:
                    UserProfile.objects.get(name=name, surname=surname, phone_number=phone)
                    id = UserProfile.objects.only('id').get(name=name, surname=surname, phone_number=phone).id
                    print("{0} {1} created before.".format(name, surname))
                except:
                    form.save()
                    id = UserProfile.objects.only('id').get(name=name, surname=surname, phone_number=phone).id
                    print('{0} {1} added to database'.format(name, surname))


                if form.cleaned_data['has_trainer'] == True:
                    return HttpResponseRedirect('/for-trainee/')
                else:
                    return HttpResponseRedirect('/not-trainer/')

            else:
                form = UserProfileForm()

    else:
        form = UserProfileForm()

    return render(request, 'user/registration.html', {'form': form})


def for_trainee(request):
    return render(request, 'user/for_trainee.html')


def payment(request):
    host = request.get_host()
    event = Event.objects.all()[Event.objects.count() - 1]

    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': event.price / 28,
        'item_name': 'Tickect id : {0}'.format(id),
        'currency_code': 'USD',
        'invoice': str(id),
        'notify_url': 'http://{}{}'.format(host,
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}/paypal_return/'.format(host),
        'cancel_return': 'http://{}/paypal_cancel/'.format(host),
    }

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'user/not_trainee.html', {'form': form})


def registration_success():
    t = turbosmsua.Turbosms(settings.TURBOSMS_LOGIN, settings.TURBOSMS_PASSWORD)
    if t.balance() < 20:
        t.send_text('Msg', [settings.CREATOR_PHONE_NUMBER], 'Поповнть рахунок на turbosms')

    try:
        send_statuses = t.send_text("Msg",
                                [phone],
                                "test")
    except:
        pass


@csrf_exempt
def paypal_return(request):
    registration_success()
    return render(request, 'paypal/paypal_return.html')


@csrf_exempt
def paypal_cancel(request):
    return render(request, 'paypal/paypal_return.html')


def landing_page(request):
    return render(request, 'landing/landing.html')
