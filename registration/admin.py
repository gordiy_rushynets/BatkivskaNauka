from django.contrib import admin
from .models import UserProfile, Message


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['name', 'surname', 'phone_number', 'has_trainer', 'paid']

    class Meta:
        model = UserProfile

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Message)
